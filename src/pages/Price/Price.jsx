import React, { useState, useEffect } from 'react';
import getData from '../../service/coinsAPI';
import PigLoading from '../../common/PigLoading/PigLoading.jsx';
import Footer from '../../common/Footer/Footer';
import CurrencyCard from './components/CurrencyCard/CurrencyCard';
import './Price.css';

function Cotation() {
  const [currency, setCurrency] = useState([]);

  useEffect(() => {
    getData(setCurrency);
  }, []);

  const cardList = () => {
    return (
      <div className="card-list">
        {currency.map((coin) => {
          return (
            <CurrencyCard
              currencyName={coin.name}
              abbreviation={coin.code}
              max={coin.high}
              min={coin.low}
              time={coin.timestamp}
            />
          );
        })}
      </div>
    );
  };

  return (
    <div className="page-wrapper">
      <div className="content-wrapper">
        <h1 className="cotation-title">Cotação das Moedas</h1>
        {currency.length > 0 ? cardList() : <PigLoading />}
      </div>
      <Footer fixed={true} />
    </div>
  );
}

export default Cotation;
