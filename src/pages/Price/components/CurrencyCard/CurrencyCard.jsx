import React from 'react';
import './CurrencyCard.css';

function CurrencyCard({ currencyName, abbreviation, max, min, time }) {
  const formatDate = (timestamp) => {
    let date = new Date(parseInt(timestamp) * 1000);
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds() === 0 ? '00' : date.getSeconds();
    let day = date.getDate();
    let month =
      date.getMonth() + 1 < 10
        ? `0${date.getMonth() + 1}`
        : date.getMonth() + 1;
    let year = date.getFullYear();

    return `Atualizado em ${day}/${month}/${year} às ${hours}:${minutes}:${seconds}`;
  };

  return (
    <div className="currency-card card-text">
      <div className="currency-title card-text">
        <p className="currency-name card-text">{currencyName}</p>
        <h3 className="currency-abb card-text">{abbreviation}</h3>
      </div>
      <div className="currency-data card-text">
        <p className="currency-values card-text">Máxima: R$ {max}</p>
        <p className="currency-values card-text">Mínima: R$ {min}</p>
        <p className="currency-time card-text">{formatDate(time)}</p>
      </div>
    </div>
  );
}

export default CurrencyCard;
