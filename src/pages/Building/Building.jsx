import React from 'react';
import pigConstruction from '../../assets/constructionPig.svg';
import Footer from '../../common/Footer/Footer';
import Button from '../../common/Button/Button';
import './Building.css';

function Building() {
  return (
    <>
      <div className="building-wrapper">
        <h1>Em construção</h1>
        <img
          className="pig-contruction"
          src={pigConstruction}
          alt="construção"
        />
        <div className="building-content">
          <p>Parece que essa página ainda não foi implementada...</p>
          <p>Tente novamente mais tarde!</p>
        </div>
        <Button text="Voltar" />
      </div>
      <Footer fixed={false} />
    </>
  );
}

export default Building;
