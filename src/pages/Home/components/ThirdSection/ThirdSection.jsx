import React from 'react';
import Button from '../../../../common/Button/Button';
import personAndPig from '../../../../assets/personAndPig.svg';
import './ThirdSection.css';

function ThirdSection() {
  return (
    <section>
      <div className="third-section-wrapper center-container">
        <div className="third-content">
          <h1>Já tem uma meta?</h1>
          <p>
            Phasellus mollis eget enim sed malesuada. Nulla facilisi. Aliquam
            nunc lacus, commodo hendrerit commodo pulvinar, suscipit eget ipsum.
            Nulla at sem ex.
          </p>
          <Button text="ver metas" nextPath="/building" />
        </div>
        <img src={personAndPig} alt="pessoa com porquinho" />
      </div>
      <div className="bar center-container">
        <h1>Não tem? Comece agora mesmo</h1>
        <Button text="nova meta" nextPath="/building" />
      </div>
    </section>
  );
}

export default ThirdSection;
