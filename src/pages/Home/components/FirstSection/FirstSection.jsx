import React from 'react';
import Button from '../../../../common/Button/Button';
import pig1 from '../../../../assets/pig1.svg';
import './FirstSection.css';

function FirstSection() {
  return (
    <section className="firt-section center-container">
      <div className="data">
        <h1>Pig Bank</h1>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quam
          officiis voluptatibus alias quibusdam id, perspiciatis reiciendis
          deserunt quis sit molestias itaque quisquam.
        </p>
        <Button text="Nova Meta" nextPath="/building" />
      </div>
      <img src={pig1} alt="porco com moeda" />
    </section>
  );
}

export default FirstSection;
