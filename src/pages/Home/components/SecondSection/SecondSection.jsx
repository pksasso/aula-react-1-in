import React from 'react';
import './SecondSection.css';
import HowToUseCard from './HowToUseCard/HowToUseCard';

function SecondSection() {
  return (
    <section className="second-section">
      <div className="center-container">
        <h1>Como usar</h1>
        <div className="list-card">
          <HowToUseCard
            img="wheelchair"
            title="defina uma meta"
            content="Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis."
          />
          <HowToUseCard
            img="objective"
            title="acompanhe o progresso"
            content="Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis."
          />
          <HowToUseCard
            img="progress"
            title="alcance seu objetivo"
            content="Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis."
          />
        </div>
      </div>
    </section>
  );
}

export default SecondSection;
