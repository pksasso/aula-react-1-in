import React from 'react';
import wheelchair from '../../../../../assets/wheelchair.svg';
import objective from '../../../../../assets/objective.svg';
import progress from '../../../../../assets/progress.svg';
import './HowToUseCard.css';

function HowToUseCard({ img, title, content }) {
  const getImage = () => {
    if (img === 'wheelchair') {
      return wheelchair;
    }
    if (img === 'objective') {
      return objective;
    }
    if (img === 'progress') {
      return progress;
    }
  };
  return (
    <div className="use-card">
      <img src={getImage()} alt={img} />
      <h1 className="title">{title}</h1>
      <p>{content}</p>
    </div>
  );
}

export default HowToUseCard;
