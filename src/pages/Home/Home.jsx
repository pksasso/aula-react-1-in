import React from 'react';
import Footer from '../../common/Footer/Footer';
import FirstSection from './components/FirstSection/FirstSection';
import SecondSection from './components/SecondSection/SecondSection';
import ThirdSection from './components/ThirdSection/ThirdSection';
import './Home.css';

function Home() {
  return (
    <div>
      <FirstSection />
      <SecondSection />
      <ThirdSection />
      <Footer fixed={false} />
    </div>
  );
}

export default Home;
