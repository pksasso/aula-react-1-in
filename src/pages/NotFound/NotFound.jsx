import React from 'react';
import confusedPig from '../../assets/confusedPig.svg';
import Footer from '../../common/Footer/Footer';
import Button from '../../common/Button/Button';
import './NotFound.css';

function NotFound() {
  return (
    <>
      <div className="not-found-wrapper">
        <h1>Erro 404</h1>
        <img className="pig-confused" src={confusedPig} alt="porco confuso" />
        <div className="not-found-content">
          <p>Ops! Parece que o endereço que você digitou está incorreto...</p>
        </div>
        <Button text="Voltar" />
      </div>
      <Footer fixed={false} />
    </>
  );
}

export default NotFound;
