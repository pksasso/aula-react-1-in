import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import NotFound from './pages/NotFound/NotFound';
import Home from './pages/Home/Home';
import Price from './pages/Price/Price';
import Building from './pages/Building/Building';
import Burger from './common/Burger/Burger';
import Menu from './common/Menu/Menu';

function Routes() {
  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <Router>
      <Burger open={menuOpen} setOpen={setMenuOpen} />
      <Menu isOpen={menuOpen} setIsOpen={setMenuOpen} />
      <Switch>
        <Route exact path="/">
          <Redirect to={'/home'} />
        </Route>
        <Route exact path="/home">
          <Home />
        </Route>
        <Route exact path="/price">
          <Price />
        </Route>
        <Route exact path="/building">
          <Building />
        </Route>
        <Route path="/">
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
}

export default Routes;
