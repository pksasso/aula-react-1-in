import axios from 'axios';

const create = axios.create({
  baseURL: 'https://economia.awesomeapi.com.br/',
});

const getData = (setCoins) => {
  create
    .get('/json/all')
    .then((resp) => {
      let arr = [];
      Object.keys(resp.data).forEach((item) => {
        arr.push(resp.data[item]);
      });
      setTimeout(() => {
        setCoins(arr);
      }, 1000);
    })
    .catch((err) => console.log(err));
};

export default getData;
