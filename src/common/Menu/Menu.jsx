import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import './Menu.css';
import Button from '../Button/Button';

function Menu({ isOpen, setIsOpen }) {
  const location = useLocation();
  return (
    <div className={`menu-wrapper ${isOpen ? 'open' : ''}`}>
      <Button
        text="inicio"
        nextPath="/home"
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        active={location.pathname === '/home' ? true : false}
      />
      <Button
        text="cotação de moedas"
        nextPath="/price"
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        active={location.pathname === '/price' ? true : false}
      />
    </div>
  );
}

export default Menu;
