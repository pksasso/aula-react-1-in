import React from 'react';
import pig from '../../assets/pigLoading.svg';
import './PigLoading.css';

function PigLoading() {
  return (
    <div className="pig-wrapper">
      <div className="pig-face">
        <img src={pig} alt="pig-loading" />
      </div>
    </div>
  );
}

export default PigLoading;
