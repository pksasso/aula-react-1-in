import React from 'react';
import { Link, useHistory, useLocation } from 'react-router-dom';
import './Button.css';

function Button({ text, nextPath, isOpen, setIsOpen, active }) {
  const history = useHistory();
  const location = useLocation();
  const selectClick = () => {
    if (setIsOpen) {
      setIsOpen(!isOpen);
    }
    if (nextPath) {
      history.push(nextPath);
    }
    if (text === 'Voltar') {
      history.goBack();
    }
  };

  return (
    <a
      className={`button-item ${active ? 'active' : ''}`}
      onClick={() => {
        selectClick();
      }}
    >
      {text}
    </a>
  );
}

export default Button;
