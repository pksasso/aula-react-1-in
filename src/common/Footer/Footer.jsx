import React from 'react';
import './Footer.css';
import confusedPig from '../../assets/confusedPig.svg';

function Footer({ fixed }) {
  const footerPosition = (fixed) => {
    if (fixed) {
      return 'footer-fixed';
    }
  };

  return (
    <>
      {fixed ? <div className="shaddow" /> : ''}
      {fixed ? (
        <img
          className="confused-pig-footer"
          src={confusedPig}
          alt="porco confuso"
        />
      ) : (
        ''
      )}
      <footer className={footerPosition(fixed)}>
        <p className="footer-text">feito com amor</p>
      </footer>
    </>
  );
}

export default Footer;
