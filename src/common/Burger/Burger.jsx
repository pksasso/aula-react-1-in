import React from 'react';
import './Burger.css';

function Burger({ open, setOpen }) {
  return (
    <button
      className="burger-button"
      open={open}
      onClick={() => setOpen(!open)}
    >
      <div className={`first-line ${open ? 'open' : ''}`} />
      <div className={`second-line ${open ? 'open' : ''}`} />
      <div className={`third-line ${open ? 'open' : ''}`} />
    </button>
  );
}

export default Burger;
